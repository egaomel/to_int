from to_int import to_int
from parameterized import parameterized
from unittest import TestCase


class TestToInt(TestCase):
    @parameterized.expand([
        [{1: 1}],
        [[1, ]],
        [(1, )],
        [{1, }],
        [bytes(1)],
    ])
    def test_1(self, value):
        """Проверка создания ошибки при вызове с аргументом неправильного типа"""

        with self.assertRaises(Exception):
            int(value)

        with self.assertRaisesRegexp(TypeError, 'не может быть приведен к типу int'):
            to_int(value)

    @parameterized.expand([
        ['1'],
        ['   1'],
        ['1   '],
        ['\n1'],
        ['1\n'],
        ['-1'],
        ['   -1'],
        ['\n-1'],
        [1.1],
        [-1.1]
    ])
    def test_3(self, value):
        """Проверка, что функции одинаково работают cо значениями, которые можно привести к типу int"""
        self.assertEqual(int(value), to_int(value))

    @parameterized.expand([
        ['1.0'],
        ['q1'],
        ['1q'],
        ['1q1'],
        ['1\n1'],
    ])
    def test_2(self, value):
        """Проверка, что функции одинаково работают c строками, которые нельзя привести к типу int"""
        with self.assertRaises(Exception):
            int(value)

        with self.assertRaisesRegexp(Exception, 'не может быть приведена к целому числу'):
            to_int(value)

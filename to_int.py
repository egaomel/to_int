def to_int(value_to_convert):
    """Привести параметр к типу int, если это возможно"""

    try:
        converter = {
            str: _str_to_int,
            int: lambda x: x,
            float: _float_to_int
        }[type(value_to_convert)]
    except KeyError:
        raise TypeError('Тип {} не может быть приведен к типу int'.format(type(value_to_convert).__str__))

    return converter(value_to_convert)


def _float_to_int(float_to_convert):
    """Перевод переменной типа float к типу int"""
    str_value = str(float_to_convert)
    return _str_to_int(str_value[:str_value.index('.')])


def _str_to_int(str_to_convert):
    """Перевод переменной типа str к типу int"""

    # очистим строку от пробелов и спецсимволов в начале и конце строки
    str_to_convert = str_to_convert.strip(' \n\t\r')

    cant_convert_exception = Exception('Строка "{}" не может быть приведена к целому числу'.format(str_to_convert))
    if len(str_to_convert) == 0:
        raise cant_convert_exception

    is_negative = False

    # проверим, является ли число отрицательным
    if str_to_convert[0] == '-':
        is_negative = True
        str_to_convert = str_to_convert[1:]

    result = 0
    numbers = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

    for character in str_to_convert:
        if character in numbers:
            result = result*10 + numbers.index(character)
        else:
            raise cant_convert_exception

    return result if not is_negative else 0-result
